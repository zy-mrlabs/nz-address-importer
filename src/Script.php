<?php namespace Boss\Address;

class Script
{
    /**
     * @param array $updatedTables
     * @return string
     */
    public static function generateCoverageRefreshStatement(array $updatedTables)
    {
        $sqlArray = [];

        if (in_array('enable', $updatedTables))
            $sqlArray[] = self::SQL_ENABLE;

        if (in_array('ultra_fast', $updatedTables))
            $sqlArray[] = self::SQL_ULTRA_FAST;

        if (in_array('chorus_nadb', $updatedTables))
            $sqlArray[] = self::SQL_CHORUS_NADB;

        if (in_array('chorus_all_svc', $updatedTables))
            $sqlArray[] = self::SQL_CHORUS_ALL_SVC;

        if (count($sqlArray) == 0)
            return null;

        $sql = 'insert into address.coverage (tui, lfc, fibre_ready_date)' . PHP_EOL;
        $sql .= implode(PHP_EOL . 'union' . PHP_EOL, $sqlArray);

        return $sql;
    }


    const SQL_ENABLE = <<<EOT
select distinct tui, 'Enable', date ''
from
(
    select tui from address.enable
    except
    select tui from address.enable_old
) a
EOT;

    const SQL_ULTRA_FAST = <<<EOT
select distinct corelogic_tui as tui, 'Ultra_Fast', date ''
from
(
    select corelogic_tui from address.ultra_fast
    except
    select corelogic_tui from address.ultra_fast_old
) a
EOT;

    const SQL_CHORUS_NADB = <<<EOT
select distinct tui, 'Chorus', date ''
from address.chorus_all_svc
where sam in
(
    select plsam from address.chorus_nadb
    except
    select plsam from address.chorus_nadb_old
)
and tui is not null
EOT;

    const SQL_CHORUS_ALL_SVC = <<<EOT
select a.tui, a.lfc, date ''
from
(
    select distinct tui, 'Chorus' as lfc
    from address.chorus_all_svc
    where sam in
    (
        select plsam
        from address.chorus_nadb
        where plsam in
        (
            select sam from address.chorus_all_svc
            except
            select sam from address.chorus_all_svc_old
        )
    )
    and tui is not null

    union

    select distinct enable.tui, 'Enable' as lfc
    from address.enable
    where enable.tui in
    (
        select tui from address.chorus_all_svc
        except
        select tui from address.chorus_all_svc_old
    )

    union

    select distinct ultra_fast.corelogic_tui as tui, 'Ultra_Fast' as lfc
    from address.ultra_fast
    where ultra_fast.corelogic_tui in
    (
        select tui from address.chorus_all_svc
        except
        select tui from address.chorus_all_svc_old
    )
) a
EOT;

}
