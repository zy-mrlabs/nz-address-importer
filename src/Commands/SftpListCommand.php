<?php namespace Boss\Address\Commands;

use Boss\Address\SftpHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SftpListCommand extends Command
{
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('sftp-list')
            ->setDescription('List files in SFTP');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $sftp = SftpHelper::fromConfig($this->config);

        $table = $this->getHelper('table');
        $table
            ->setHeaders(['Modified', 'Filename', 'Identifier', 'Size'])
            ->setRows($sftp->listFiles());
        $table->render($output);
    }
}
