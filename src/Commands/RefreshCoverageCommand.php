<?php namespace Boss\Address\Commands;

use Boss\Address\Script;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshCoverageCommand extends Command
{
    public function configure()
    {
        $this->setName('refresh-coverage')
            ->setDescription('Refresh coverage (enable chorus_nadb ultra_fast chorus_all_svc)')
            ->addArgument('tables', InputArgument::REQUIRED | InputArgument::IS_ARRAY,
                'Affected tables (enable chorus_nadb ultra_fast chorus_all_svc)');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(Script::generateCoverageRefreshStatement($input->getArgument('tables')));
    }
}
