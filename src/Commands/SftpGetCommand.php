<?php namespace Boss\Address\Commands;

use Boss\Address\SftpHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SftpGetCommand extends Command
{
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('sftp-get')
            ->setDescription('Get files from SFTP (Enable.zip NADB_Extract.zip Ultra_Fast.zip)')
            ->addArgument('destination', InputArgument::REQUIRED, 'Destination path')
            ->addArgument('files', InputArgument::REQUIRED | InputArgument::IS_ARRAY,
                'Files to download (Enable.zip NADB_Extract.zip Ultra_Fast.zip)');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $sftp = SftpHelper::fromConfig($this->config);
        $sftp->download($input->getArgument('files'), $input->getArgument('destination'));
    }
}
