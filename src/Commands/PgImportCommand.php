<?php namespace Boss\Address\Commands;

use Boss\Address\AddressImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PgImportCommand extends Command
{
    private $connectionStrings;

    public function __construct(array $connectionStrings)
    {
        $this->connectionStrings = $connectionStrings;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('import')
            ->setDescription('Import zip file to PostgreSQL database')
            ->addArgument('file', InputArgument::REQUIRED, 'Address zip file')
            ->addOption('production');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $environment = $input->getOption('production') ? 'production' : 'development';
        $zipFileName = $input->getArgument('file');
        $connectionString = $this->connectionStrings[$environment];

        $start = microtime(true);
        $addressImporter = new AddressImporter;
        $addressImporter->import($zipFileName, $connectionString);
        $output->writeln(sprintf('<info>%.3f seconds</info>', microtime(true) - $start));
    }
}
