<?php namespace Boss\Address;

use Exception;
use Net_SFTP;

class SftpHelper
{
    public static function fromConfig(array $config)
    {
        return new SftpHelper(
            $config['host'],
            $config['username'],
            $config['password'],
            $config['directory']
        );
    }

    private $host;
    private $username;
    private $password;
    private $directory;

    /**
     * SftpHelper constructor.
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $directory
     */
    public function __construct($host, $username, $password, $directory)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->directory = $directory;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function listFiles()
    {
        $mapping = [
            'chorus_all_svc.zip' => 'chorus_all_svc',
            'Enable.zip' => 'enable',
            'NADB_Extract.zip' => 'chorus_nadb',
            'Ultra_Fast.zip' => 'ultra_fast'
        ];

        $sftp = new Net_SFTP($this->host);

        if (!$sftp->login($this->username, $this->password))
            throw new Exception('Login Failed');

        $items = $sftp->rawlist($this->directory);

        // $acceptedFiles = array_keys($mapping);

        // $items = array_filter($items, function ($item) use ($acceptedFiles) {
        //     return in_array($item['filename'], $acceptedFiles);
        // });

        foreach ($items as $key => $item) {
            $sortColumn[$key] = $item['mtime'];
        }
        array_multisort($sortColumn, SORT_DESC, $items);

        return array_map(function ($item) use ($mapping) {
            $filename = $item['filename'];
            return [
                date('Y-m-d H:i:s', $item['mtime']),
                $filename,
                $mapping[$filename],
                $item['size']
            ];
        }, $items);
    }

    /**
     * @param array $fileNames
     * @param string $localDirectory
     * @throws Exception
     */
    public function download(array $fileNames, $localDirectory)
    {
        $sftp = new Net_SFTP($this->host);

        if (!$sftp->login($this->username, $this->password))
            throw new Exception('Login Failed');

        $sftp->chdir($this->directory);

        foreach ($fileNames as $fileName) {
            echo 'downloading ' . $fileName . PHP_EOL;
            $sftp->get($fileName, $localDirectory . DIRECTORY_SEPARATOR . $fileName);
        }
    }

}
