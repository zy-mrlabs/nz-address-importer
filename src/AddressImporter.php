<?php namespace Boss\Address;

use Exception;
use InvalidArgumentException;
use ZipArchive;

class AddressImporter
{
    /**
     * @param string $zipFileName
     * @param string $connectionString
     * @throws Exception
     */
    public function import($zipFileName, $connectionString)
    {
        $csvFileName = static::getFilenameAtIndex($zipFileName, 0);
        $uri = 'zip://' . $zipFileName . '#' . $csvFileName;
        $tableName = static::toTableName(basename($csvFileName, '.csv'));
        echo $tableName . PHP_EOL;

        static::usingFile($uri, function ($handle) use ($tableName, $connectionString) {
            fgetcsv($handle); // header row
            static::usingPgConnection($connectionString, function ($conn) use ($tableName, $handle) {
                pg_query($conn, 'begin');
                pg_query($conn, 'drop table if exists ' . $tableName . '_old');
                pg_query($conn, 'create table ' . $tableName . '_old as select * from ' . $tableName);
                pg_query($conn, 'truncate table ' . $tableName);
                pg_query($conn, 'copy ' . $tableName . ' from stdin');

                $row = 1;
                while ($data = fgetcsv($handle)) {
                    pg_put_line($conn, implode("\t", $data) . "\n");
                    echo number_format($row++) . " rows\r";
                }
                echo PHP_EOL;

                pg_put_line($conn, "\\.\n");
                pg_end_copy($conn);
                pg_query($conn, 'commit');
            });
        });
    }

    /**
     * @param string $filename
     * @param callback $callback
     * @throws Exception
     */
    private static function usingFile($filename, $callback)
    {
        $handle = fopen($filename, 'r');
        if ($handle === false)
            throw new Exception('Unable to open file: ' . $filename);
        try {
            $callback($handle);
        } finally {
            fclose($handle);
        }
    }

    /**
     * @param string $connectionString
     * @param callback $callback
     * @throws Exception
     */
    private static function usingPgConnection($connectionString, $callback)
    {
        $conn = pg_connect($connectionString);
        if ($conn === false)
            throw new Exception('Unable to connect to database: ' . $connectionString);
        try {
            $callback($conn);
        } finally {
            pg_close($conn);
        }
    }

    /**
     * @param string $zipFileName
     * @param int $index
     * @return string
     * @throws Exception
     */
    private static function getFilenameAtIndex($zipFileName, $index)
    {
        $zipArchive = new ZipArchive();

        if ($zipArchive->open($zipFileName) !== true)
            throw new Exception('Unable to open zip: ' . $zipFileName);

        $name = $zipArchive->statIndex($index)['name'];
        $zipArchive->close();
        return $name;
    }

    /**
     * @param string $basename
     * @return string
     */
    private static function toTableName($basename)
    {
        switch ($basename) {
            case 'NADB_Extract':
                return 'address.chorus_nadb';
            case 'Enable':
                return 'address.enable';
            case 'Ultra_Fast':
                return 'address.ultra_fast';
        }
        throw new InvalidArgumentException('Unrecognized basename: ' . $basename);
    }
}
